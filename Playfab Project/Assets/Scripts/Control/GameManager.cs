﻿using Player;
using Menu;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using Enemies;
using PlayerData;
using UI;

namespace Control
{
    public class GameManager : Singleton<GameManager>
    {
        public static event Action OnServerLogin;

        public string GameVersion;

        private void Awake()
        {
            OnServerLogin += LoadServerData;
        }

        private void OnDestroy()
        {
            OnServerLogin -= LoadServerData;
        }

        private void Start()
        {
            ServerLogin();
        }

        private void ServerLogin()
        {
            StartUIController.Instance.SetConnectionStatusText("Logging in...", false);
            PlayFabManager.Instance.Login(OnloginSuccess, OnLoginFailed);
        }

        private void OnloginSuccess(LoginResult loginResult)
        {
            Debug.Log("User login: " + loginResult.PlayFabId);
            Debug.Log("User newly created: " + loginResult.NewlyCreated);

            if (loginResult.NewlyCreated)
            {
                StartUIController.Instance.SetConnectionStatusText("Creating account...", false);
                PlayFabManager.Instance.CreateAccount(
                    () => OnServerLogin?.Invoke(),
                    () => StartUIController.Instance.SetConnectionStatusText("Account creation failed. Please restart.", true)
                );
            }
            else
            {
                OnServerLogin?.Invoke();
            }
        }

        private void OnLoginFailed(PlayFabError error)
        {
            StartUIController.Instance.SetConnectionStatusText("Failed to connect to the server. Please try again.", true);
            Debug.LogError("Login failed: " + error.ErrorMessage);
        }

        #region Load Server Data

        public void LoadServerData()
        {
            StartUIController.Instance.SetConnectionStatusText("Loading game data...", false);
            PlayFabManager.Instance.GetTitleData();
        }

        public void LoadGameSetup(Dictionary<string, string> data)
        {
            SetPlayFabVersion(data["ClientVersion"]);
            SetEnemyDatabase(data["EnemyDatabase"]);
            SetExpDatabase(data["ExpDatabase"]);
            SetPlayerStatsDatabase(data["PlayerStatsDatabase"]);
            SetPriceDatabase(data["PriceDatabase"]);

            StartUIController.Instance.SetConnectionStatusText("Loading player data...", false);
            PlayFabManager.Instance.GetPlayerData();    // El player data tiene que cargarse después
        }

        private void SetPlayFabVersion(string version)
        {
            GameVersion = version;
        }

        private void SetEnemyDatabase(string databaseJSON)
        {
            EnemyContainer enemyData = JsonUtility.FromJson<EnemyContainer>("{\"EnemyList\":" + databaseJSON + "}");
            EnemyDatabase.Instance.Initialize(enemyData.EnemyList);
        }

        private void SetExpDatabase(string databaseJSON)
        {
            ExpContainer expData = JsonUtility.FromJson<ExpContainer>("{\"ExpPerLevel\":" + databaseJSON + "}");
            ExpDatabase.Instance.Initialize(expData.ExpPerLevel);
        }

        private void SetPlayerStatsDatabase(string databaseJSON)
        {
            PlayerStatsContainer statsData = JsonUtility.FromJson<PlayerStatsContainer>("{\"StatsPerLevel\":" + databaseJSON + "}");
            PlayerStatsDatabase.Instance.Initialize(statsData.StatsPerLevel);
        }

        private void SetPriceDatabase(string databaseJSON)
        {
            ItemContainer itemData = JsonUtility.FromJson<ItemContainer>("{\"ItemList\":" + databaseJSON + "}");
            PriceDatabase.Instance.Initialize(itemData.ItemList);
        }



        public void LoadPlayerSetup(Dictionary<string, UserDataRecord> data)
        {
            PlayerModel playerData = PlayerModel.LoadPlayerData(data);

            LevelManager.Instance.InitializeLevel(playerData.CurrentLevel, playerData.CurrentExp);
            StageManager.Instance.InitializeStage(playerData.CurrentStage);
            EconomyManager.Instance.InitializeEconomy(playerData.CurrentMoney);
            ShipEquipment.Instance.InitializeCurrentEquipment(playerData.CurrentEquipment.EquipmentList);
            ShopManager.Instance.InitializeUnlockedEquipment(playerData.UnlockedEquipment.EquipmentList);

            StartUIController.Instance.SetConnectionStatusText("Game Loaded!", false);
            StartUIController.Instance.EnableStartButton();
        }

        #endregion

        #region Update Server Data

        public void UpdateDataAfterWave()
        {
            Dictionary<string, string> data = new Dictionary<string, string>
            {
                {"Exp", LevelManager.Instance.CurrentExp.ToString()},
                {"Level", LevelManager.Instance.CurrentLevel.ToString()},
                {"Money", EconomyManager.Instance.CurrentMoney.ToString()},
                {"Stage", StageManager.Instance.CurrentStage.ToString()}
            };

            PlayFabManager.Instance.UpdatePlayerData(data);
        }

        public void UpdateDataAfterBuy(List<EquipmentType> unlockedEquipment)
        {
            string equipmentJson = "[";

            for (int i = 0; i < unlockedEquipment.Count; i++)
            {
                if (i == unlockedEquipment.Count - 1)
                {
                    equipmentJson += "\"" + unlockedEquipment[i].ToString() + "\"";
                }
                else
                {
                    equipmentJson += "\"" + unlockedEquipment[i].ToString() + "\", ";
                }
            }

            equipmentJson += "]";


            Dictionary<string, string> data = new Dictionary<string, string>
                {
                    {"UnlockedEquipment", equipmentJson},
                    {"Money", EconomyManager.Instance.CurrentMoney.ToString() }
                };

            PlayFabManager.Instance.UpdatePlayerData(data);
        }

        public void UpdateDataAfterEquip()
        {
            Dictionary<string, string> data = new Dictionary<string, string>
                {
                    {"CurrentEquipment", "[\"" + ShipEquipment.Instance.ShootEquipped + "\", \"" + ShipEquipment.Instance.DefenseEquipped + "\", \"" + ShipEquipment.Instance.ActiveEquipped + "\"]"},
                };

            PlayFabManager.Instance.UpdatePlayerData(data);
        }

        #endregion

        #region Containers

        [Serializable]
        public class EnemyContainer
        {
            public List<EnemyData> EnemyList;
        }

        [Serializable]
        public class ExpContainer
        {
            public List<int> ExpPerLevel;
        }

        [Serializable]
        public class PlayerStatsContainer
        {
            public List<PlayerStats> StatsPerLevel;
        }

        [Serializable]
        public class ItemContainer
        {
            public List<ItemData> ItemList;
        }

        #endregion
    }
}