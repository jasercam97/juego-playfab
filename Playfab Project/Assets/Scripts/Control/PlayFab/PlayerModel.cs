﻿using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Control
{
    [Serializable]
    public class PlayerModel
    {
        public int CurrentLevel;
        public int CurrentExp;
        public int CurrentMoney;
        public int CurrentStage;
        public EquipmentContainer UnlockedEquipment;
        public EquipmentContainer CurrentEquipment;

        private void SetLevel(string level)
        {
            CurrentLevel = int.Parse(level);
        }

        private void SetExp(string exp)
        {
            CurrentExp = int.Parse(exp);
        }

        private void SetMoney(string money)
        {
            CurrentMoney = int.Parse(money);
        }

        private void SetStage(string stage)
        {
            CurrentStage = int.Parse(stage);
        }

        private void SetUnlockedEquipment(string unlocksJSON)
        {
            UnlockedEquipment = JsonUtility.FromJson<EquipmentContainer>("{\"EquipmentList\":" + unlocksJSON + "}");
        }

        private void SetCurrentEquipment(string equipmentJSON)
        {
            CurrentEquipment = JsonUtility.FromJson<EquipmentContainer>("{\"EquipmentList\":" + equipmentJSON + "}");
        }

        public static PlayerModel LoadPlayerData(Dictionary<string, UserDataRecord> data)
        {
            PlayerModel model = new PlayerModel();

            model.SetLevel(data["Level"].Value);
            model.SetExp(data["Exp"].Value);
            model.SetMoney(data["Money"].Value);
            model.SetStage(data["Stage"].Value);
            model.SetUnlockedEquipment(data["UnlockedEquipment"].Value);
            model.SetCurrentEquipment(data["CurrentEquipment"].Value);

            return model;
        }

        [System.Serializable]
        public class EquipmentContainer
        {
            public List<string> EquipmentList;
        }
    }
}