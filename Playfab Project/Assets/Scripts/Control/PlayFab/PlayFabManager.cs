﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Control
{
    public class PlayFabManager : Singleton<PlayFabManager>
    {
        #region LOGIN

        public void Login(Action<LoginResult> onSuccess, Action<PlayFabError> onFail)
        {
            var request = new LoginWithCustomIDRequest
            {
                CreateAccount = true,
                CustomId = SystemInfo.deviceUniqueIdentifier
            };

            PlayFabClientAPI.LoginWithCustomID(request, onSuccess, onFail);
        }

        #endregion

        #region Get Data

        public void GetTitleData()
        {
            var request = new GetTitleDataRequest();

            PlayFabClientAPI.GetTitleData(request,
                (result) =>
                {
                    GameManager.Instance.LoadGameSetup(result.Data);
                },
                (error) =>
                {
                    Debug.LogError("Error getting user data: " + error.GenerateErrorReport());
                }
            );
        }

        public void GetPlayerData()
        {
            var request = new GetUserDataRequest();

            PlayFabClientAPI.GetUserData(request,
                (result) =>
                {
                    GameManager.Instance.LoadPlayerSetup(result.Data);
                },
                (error) =>
                {
                    Debug.LogError("Error getting user data: " + error.GenerateErrorReport());
                }
            );
        }

        #endregion

        #region Update Data

        public void UpdatePlayerData(Dictionary<string, string> data)
        {
            var request = new UpdateUserDataRequest()
            {
                Data = data
            };

            PlayFabClientAPI.UpdateUserData(request,
                (result) =>
                {
                    Debug.Log("User data updated");
                },
                (error) =>
                {
                    Debug.LogError("Error updating user data: " + error.GenerateErrorReport());
                });
        }

        #endregion

        #region Create Account

        // Inicializa la base de datos del jugador
        public void CreateAccount(Action onSuccess, Action onFail)
        {
            Dictionary<string, string> data = new Dictionary<string, string>
                {
                    {"Level", "0"},
                    {"Exp", "0"},
                    {"Stage", "1"},
                    {"Money", "0"},
                    {"UnlockedEquipment", "[\"" + EquipmentType.NormalShot.ToString() + "\", \"" + EquipmentType.EnergyPulse.ToString() + "\"]"},
                    {"CurrentEquipment", "[\"" + EquipmentType.NormalShot.ToString() + "\", \"None\", \"" + EquipmentType.EnergyPulse.ToString() + "\"]"}
                };

            var request = new UpdateUserDataRequest()
            {
                Data = data
            };

            PlayFabClientAPI.UpdateUserData(request,
                result =>
                {
                    onSuccess();
                },
                error =>
                {
                    onFail();
                }
            );
        }

        #endregion
    }
}