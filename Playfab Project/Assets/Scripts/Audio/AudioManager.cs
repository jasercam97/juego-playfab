﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Utils;

namespace Audio
{
    public class AudioManager : Singleton<AudioManager>
    {
        public const string KEY_SOUND_ACTIVE = "SoundActive";
        public const string KEY_MUSIC_ACTIVE = "MusicActive";

        [Header("UI sounds")]
        public AudioSource InterfaceAS;
        public AudioClip ButtonClickSound;
        public AudioClip EquipSound;
        public AudioClip ConfirmSound;

        [Header("Shop sounds")]
        public AudioSource ShopAS;
        public AudioClip BuySound;
        public AudioClip CantBuySound;

        [Header("Death sounds")]
        public AudioSource DeathAS;
        public AudioClip PlayerDeathSound;
        public AudioClip EnemyDeathSound;
        public AudioClip BossDeathSound;

        [Header("Bullet pop sound")]
        public AudioSource BulletAS;
        public AudioClip BulletPopSound;

        [Header("Other sounds")]
        public AudioSource OtherAS;
        public AudioClip StageCompleteSound;
        public AudioClip LevelUpSound;

        [Header("Game music")]
        public float MusicTransitionTime;
        public AudioSource MenuMusicAS;
        public AudioSource GameMusicAS;
        public AudioSource BossMusicAS;
        public AudioMixerSnapshot MenuSnapshot;
        public AudioMixerSnapshot GameSnapshot;
        public AudioMixerSnapshot BossSnapshot;

        [Header("Volume control")]
        public AudioMixer MainAudioMixer;

        private void Start()
        {
            PlayMenuMusic();

            SetMusicEnabled(PlayerPrefs.GetInt(KEY_MUSIC_ACTIVE, 1) == 1);
            SetSoundEnabled(PlayerPrefs.GetInt(KEY_SOUND_ACTIVE, 1) == 1);
        }


        private void PlaySound(AudioSource audioSource, AudioClip sound, float minPitch, float maxPitch)
        {
            audioSource.pitch = Random.Range(minPitch, maxPitch);
            audioSource.PlayOneShot(sound);
        }

        // UI sounds
        public void PlayButtonClickSound()
        {
            PlaySound(InterfaceAS, ButtonClickSound, 0.85f, 1.15f);
        }

        public void PlayEquipSound()
        {
            PlaySound(InterfaceAS, EquipSound, 0.9f, 1.1f);
        }

        public void PlayConfirmSound()
        {
            PlaySound(InterfaceAS, ConfirmSound, 0.98f, 1.02f);
        }


        // Shop sounds
        public void PlayBuySound()
        {
            PlaySound(ShopAS, BuySound, 0.99f, 1.01f);
        }

        public void PlayCantBuySound()
        {
            PlaySound(ShopAS, CantBuySound, 0.98f, 1.02f);
        }


        // Death sounds
        public void PlayPlayerDeathSound()
        {
            PlaySound(DeathAS, PlayerDeathSound, 0.95f, 1.05f);
        }

        public void PlayEnemyDeathSound()
        {
            PlaySound(DeathAS, EnemyDeathSound, 0.8f, 1.2f);
        }

        public void PlayBossDeathSound()
        {
            PlaySound(DeathAS, BossDeathSound, 0.95f, 1.05f);
        }


        // Bullet sounds
        public void PlayBulletPopSound()
        {
            PlaySound(BulletAS, BulletPopSound, 0.85f, 1.15f);
        }


        // Other sounds
        public void PlayStageCompleteSound()
        {
            PlaySound(OtherAS, StageCompleteSound, 0.98f, 1.02f);
        }

        public void PlayLevelUpSound()
        {
            PlaySound(OtherAS, LevelUpSound, 0.98f, 1.02f);
        }

        // Music
        public void TransitionToMenuMusic()
        {
            if (!MenuMusicAS.isPlaying)
            {
                MenuSnapshot.TransitionTo(MusicTransitionTime);
                Invoke(nameof(PlayMenuMusic), MusicTransitionTime);
            }
        }

        public void TransitionToGameMusic()
        {
            if (!GameMusicAS.isPlaying)
            {
                GameSnapshot.TransitionTo(MusicTransitionTime);
                Invoke(nameof(PlayGameMusic), MusicTransitionTime);
            }
        }

        public void TransitionToBossMusic()
        {
            if (!BossMusicAS.isPlaying)
            {
                BossSnapshot.TransitionTo(MusicTransitionTime);
                Invoke(nameof(PlayBossMusic), MusicTransitionTime);
            }
        }

        public void PlayMenuMusic()
        {
            MenuMusicAS.Play();
            GameMusicAS.Stop();
            BossMusicAS.Stop();
        }

        public void PlayGameMusic()
        {
            MenuMusicAS.Stop();
            GameMusicAS.Play();
            BossMusicAS.Stop();
        }

        public void PlayBossMusic()
        {
            MenuMusicAS.Stop();
            GameMusicAS.Stop();
            BossMusicAS.Play();
        }


        public void SetSoundEnabled(bool enabled)
        {
            if (enabled)
            {
                MainAudioMixer.SetFloat("SoundVolume", Mathf.Log10(1f) * 20);
                PlayerPrefs.SetInt(KEY_SOUND_ACTIVE, 1);
            }
            else
            {
                MainAudioMixer.SetFloat("SoundVolume", Mathf.Log10(0.00001f) * 20);
                PlayerPrefs.SetInt(KEY_SOUND_ACTIVE, 0);
            }
        }

        public void SetMusicEnabled(bool enabled)
        {
            if (enabled)
            {
                MainAudioMixer.SetFloat("MusicVolume", Mathf.Log10(1f) * 20);
                PlayerPrefs.SetInt(KEY_MUSIC_ACTIVE, 1);
            }
            else
            {
                MainAudioMixer.SetFloat("MusicVolume", Mathf.Log10(0.0001f) * 20);
                PlayerPrefs.SetInt(KEY_MUSIC_ACTIVE, 0);
            }
        }
    }
}