﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class TextVanish : MonoBehaviour
    {
        public float VanishDelay;

        private void Start()
        {
            Invoke(nameof(Vanish), VanishDelay);
        }

        private void Vanish()
        {
            gameObject.SetActive(false);
        }
    }
}