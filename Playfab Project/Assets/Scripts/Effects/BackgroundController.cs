﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class BackgroundController : MonoBehaviour
    {
        public float MoveSpeed;
        public Transform SiblingStartPoint;


        private void Update()
        {
            transform.Translate(Vector3.down * MoveSpeed * Time.deltaTime, Space.World);
        }


        private void OnBecameInvisible()
        {
            RestartPosition();
        }

        private void RestartPosition()
        {
            transform.position = SiblingStartPoint.position;
        }
    }
}