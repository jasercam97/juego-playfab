﻿using Menu;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace PlayerData
{
    public class EconomyManager : Singleton<EconomyManager>
    {
        public int CurrentMoney { get; set; }

        public void UpdateMoney(int amount)
        {
            CurrentMoney += amount;
            MenuManager.Instance.SetMoneyText();
        }

        public void InitializeEconomy(int money)
        {
            CurrentMoney = money;
        }
    }
}