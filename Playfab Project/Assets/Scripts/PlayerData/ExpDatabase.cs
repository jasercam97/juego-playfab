﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace PlayerData
{
    public class ExpDatabase : Singleton<ExpDatabase>
    {
        public List<int> ExpForLevel;

        public void Initialize(List<int> expList)
        {
            ExpForLevel.AddRange(expList);
        }
    }
}