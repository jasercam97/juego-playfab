﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace PlayerData
{
    public class StageManager : Singleton<StageManager>
    {
        public int CurrentStage;
        public int TotalStages;

        public void InitializeStage(int stage)
        {
            CurrentStage = stage;
        }
    }
}