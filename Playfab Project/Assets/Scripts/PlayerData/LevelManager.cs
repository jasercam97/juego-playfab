﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace PlayerData
{
    public class LevelManager : Singleton<LevelManager>
    {
        public int CurrentLevel { get; set; }
        public int CurrentExp { get; set; }
        public bool IsMaxLevel { get { return CurrentLevel == _maxLevel; } }

        private int _neededExp;
        private int _maxLevel;

        public void InitializeLevel(int level, int exp)
        {
            CurrentLevel = level;
            CurrentExp = exp;

            _maxLevel = ExpDatabase.Instance.ExpForLevel.Count - 1;
            _neededExp = CalculateNeededExp();
        }

        public void IncreaseExp(int expValue)
        {
            CurrentExp += expValue;

            if (CurrentExp >= _neededExp && CurrentLevel < _maxLevel)
            {
                IncreaseLevel();
            }
        }

        private void IncreaseLevel()
        {
            CurrentLevel++;

            CurrentExp -= _neededExp;

            _neededExp = CalculateNeededExp();

            // Comprobación por si se sube más de un nivel a la vez
            if (CurrentExp >= _neededExp && CurrentLevel < _maxLevel)
            {
                IncreaseLevel();
            }
        }

        public float GetExpPercent()
        {
            return IsMaxLevel? 1 : (float) CurrentExp / _neededExp;
        }

        private int CalculateNeededExp()
        {
            if (CurrentLevel < _maxLevel)
            {
                return ExpDatabase.Instance.ExpForLevel[CurrentLevel];
            }
            else
            {
                return 1;
            }
        }
    }
}