﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{
    public class EnemyHealthController : MonoBehaviour
    {
        private float _maxHealth;
        private float _currentHealth;

        public void InitializeHealth(float health)
        {
            _maxHealth = health;
        }

        public void SetFull()
        {
            _currentHealth = _maxHealth;
        }

        /// <summary>
        /// Devuelve true si muere al recibir el golpe
        /// </summary>
        /// <param name="damage">Daño recibido</param>
        /// <returns></returns>
        public bool ReceiveDamage(float damage)
        {
            _currentHealth -= damage;

            if (_currentHealth <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}