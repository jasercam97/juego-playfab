﻿public enum EnemyMovementType
{
    None, Linear, LoopRight, LoopLeft, StopAndGo, DiagonalRight, DiagonalLeft, LeftRight, QuarterRight, QuarterLeft,
}