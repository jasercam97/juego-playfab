﻿using Bullets;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Enemies
{
    public class EnemyShootController : MonoBehaviour
    {
        public ShootType TypeOfShot;
        public float ShootRate;
        public float FirstShotDelay;

        [Header("Normal Shot")]
        public Transform NormalShotPosition;

        [Header("Double Shot")]
        public List<Transform> DoubleShotPositions;

        [Header("Triple Shot")]
        public List<Transform> TripleShotPositions;

        [Header("Bullet pool")]
        public Transform BulletPoolParent;
        public GameObject BulletPrefab;

        private float _damage;
        private Bullet _currentBullet;
        private bool _curveDir = true;

        private ShooterEnemy _parentEnemy;

        private void Awake()
        {
            _parentEnemy = GetComponent<ShooterEnemy>();

            if (BulletPoolParent == null)
            {
                BulletPoolParent = GameObject.FindGameObjectWithTag("EnemyBulletParent").transform;
            }
        }

        public void InitializeShots(float damage)
        {
            _damage = damage;
        }

        public void InvokeShots()
        {
            switch (TypeOfShot)
            {
                case ShootType.Default:
                    break;

                case ShootType.NormalShot:
                    InvokeRepeating(nameof(DoNormalShot), ShootRate + FirstShotDelay, ShootRate);
                    break;

                case ShootType.DoubleShot:
                    InvokeRepeating(nameof(DoDoubleShot), ShootRate + FirstShotDelay, ShootRate);
                    break;

                case ShootType.TripleShot:
                    InvokeRepeating(nameof(DoTripleShot), ShootRate + FirstShotDelay, ShootRate);
                    break;

                case ShootType.CurveShot:
                    InvokeRepeating(nameof(DoCurvedShot), ShootRate + FirstShotDelay, ShootRate);
                    break;
            }
        }

        private void DoNormalShot()
        {
            _parentEnemy.DoShootSound();

            _currentBullet = PoolManager.Instance.GetFreeObject(BulletPoolParent, BulletPrefab).GetComponent<Bullet>();
            SpawnBullet(_currentBullet, NormalShotPosition);
        }

        private void DoDoubleShot()
        {
            DoMultiShot(DoubleShotPositions);
        }

        private void DoTripleShot()
        {
            DoMultiShot(TripleShotPositions);
        }

        private void DoMultiShot(List<Transform> ShotPositions)
        {
            _parentEnemy.DoShootSound();

            foreach (Transform spawnPoint in ShotPositions)
            {
                _currentBullet = PoolManager.Instance.GetFreeObject(BulletPoolParent, BulletPrefab).GetComponent<Bullet>();
                SpawnBullet(_currentBullet, spawnPoint);
            }
        }

        private void DoCurvedShot()
        {
            _parentEnemy.DoShootSound();

            _currentBullet = PoolManager.Instance.GetFreeObject(BulletPoolParent, BulletPrefab).GetComponent<Bullet>();
            _currentBullet.GetComponent<CurveBullet>().SetStartDirection(_curveDir);

            _curveDir = !_curveDir;
            SpawnBullet(_currentBullet, NormalShotPosition);
        }

        private void SpawnBullet(Bullet bullet, Transform spawnPoint)
        {
            bullet.gameObject.SetActive(true);
            bullet.Initialize(_damage, spawnPoint, TypeOfShot);
        }

        public void CancelShots()
        {
            CancelInvoke();
        }
    }
}