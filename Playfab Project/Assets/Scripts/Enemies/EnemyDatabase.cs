﻿using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Enemies
{
    [System.Serializable]
    public class EnemyDatabase : Singleton<EnemyDatabase>
    {
        public List<EnemyData> EnemyList;

        public void Initialize(List<EnemyData> enemyData)
        {
            EnemyList.AddRange(enemyData);

            foreach (EnemyData enemy in EnemyList)
            {
                enemy.Type = EnumUtils.Instance.FindEnumValue<EnemyType>(enemy.Name);
            }
        }

        public EnemyData GetEnemyData(EnemyType enemyType)
        {
            return EnemyList.Find(enemy => enemy.Type == enemyType);
        }
    }

    [System.Serializable]
    public class EnemyData
    {
        public string Name;
        public EnemyType Type;
        public float Health;
        public float ShootDamage;
        public float CollisionDamage;
        public float MoveSpeed;
        public int ExpReward;
        public int MoneyReward;
    }
}