﻿using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Enemies
{
    public class ShooterEnemy : BasicEnemy
{
    [Header("Shoot controller")]
    public List<EnemyShootController> ShootControllers;

    [Header("Shoot sounds")]
    public AudioSource ShootAS;
    public AudioClip ShootSound;


    protected override void Initialize()
    {
        base.Initialize();

        foreach (EnemyShootController shootController in ShootControllers)
        {
            shootController.InitializeShots(_enemyData.ShootDamage);
        }
    }

    public override void Revive(EnemyMovementType movementType)
    {
        base.Revive(movementType);

        foreach (EnemyShootController shootController in ShootControllers)
        {
            shootController.InvokeShots();
        }
    }

    protected override void DisableObject()
    {
        foreach (EnemyShootController shootController in ShootControllers)
        {
            shootController.CancelShots();
        }

        base.DisableObject();
    }

    public void DoShootSound()
    {
        ShootAS.pitch = Random.Range(0.8f, 1.2f);
        ShootAS.PlayOneShot(ShootSound);
    }
}
}