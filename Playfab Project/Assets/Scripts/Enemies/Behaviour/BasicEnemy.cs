﻿using Audio;
using Bullets;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Waves;

namespace Enemies
{
    public class BasicEnemy : MonoBehaviour
    {
        public EnemyType TypeOfEnemy;
        public bool IsBoss;
        public EnemyHealthController HealthController;
        public EnemyMovementController MovementController;

        [Header("Effects")]
        public ParticleSystem DeathParticles;
        public Transform ParticlesSpawnPoint;
        public SpriteRenderer ShipSprite;
        public Color GetHitColor;

        [Header("Health Sound")]
        public AudioSource HealthAS;
        public AudioClip GetHitSound;

        protected EnemyData _enemyData;
        protected float _collisionDamage;

        private void Awake()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            _enemyData = EnemyDatabase.Instance.GetEnemyData(TypeOfEnemy);

            HealthController.InitializeHealth(_enemyData.Health);
            MovementController.SetMoveSpeed(_enemyData.MoveSpeed);
            _collisionDamage = _enemyData.CollisionDamage;
        }

        public virtual void Revive(EnemyMovementType movementType)
        {
            HealthController.SetFull();
            MovementController.SetMovementType(movementType);
            MovementController.DoPirouette();
        }

        public void GetHit(float damage)
        {
            if (HealthController.ReceiveDamage(damage)) // Si sale true es que ha muerto
            {
                Die();
            }
            else
            {
                Active.Instance.IncreaseChargeByHit();

                DoGetHitSound();
                ShipSprite.color = GetHitColor;
                CancelInvoke(nameof(SetNormalColor));
                Invoke(nameof(SetNormalColor), 0.1f);
            }
        }

        private void SetNormalColor()
        {
            ShipSprite.color = Color.white;
        }

        private void Die()
        {
            // TODO Efecto de morir
            if (IsBoss)
            {
                AudioManager.Instance.PlayBossDeathSound();
            }
            else
            {
                AudioManager.Instance.PlayEnemyDeathSound();
                Active.Instance.IncreaseChargeByKill();
            }

            Instantiate(DeathParticles, ParticlesSpawnPoint.position, transform.rotation);

            WaveManager.Instance.AddToRewards(_enemyData.ExpReward, _enemyData.MoneyReward);
            MovementController.KillSequence();
            DisableObject();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Bullet"))
            {
                GetHit(collision.GetComponent<Bullet>().Damage);
            }
        }

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.CompareTag("Player"))
            {
                collision.collider.GetComponent<PlayerHealth>().GetHit(_collisionDamage, true);
                GetHit(50);
            }
        }

        private void OnBecameInvisible()
        {
            DisableObject();
        }

        protected virtual void DisableObject()
        {
            WaveManager.Instance?.RemoveFromAliveList(gameObject);
            gameObject.SetActive(false);
        }

        private void DoGetHitSound()
        {
            HealthAS.pitch = Random.Range(0.8f, 1.2f);
            HealthAS.PlayOneShot(GetHitSound);
        }
    }
}