﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using Waves;

namespace Enemies
{
    public class EnemySpawner : Singleton<EnemySpawner>
    {
        public List<Transform> SpawnPoints;

        [Header("POOLING")]
        public GameObject BasicEnemyPrefab;
        public Transform BasicEnemyParent;

        [Space(10)]
        public GameObject NormalShotPrefab;
        public Transform NormalShotParent;

        [Space(10)]
        public GameObject CurveShotPrefab;
        public Transform CurveShotParent;

        [Space(10)]
        public GameObject DoubleShotPrefab;
        public Transform DoubleShotParent;

        [Space(10)]
        public GameObject TripleShotPrefab;
        public Transform TripleShotParent;

        [Header("BOSSES")]
        public GameObject Miniboss1;
        public GameObject Boss1;

        private GameObject _currentEnemy;

        public void SpawnEnemy(EnemyType type, int spawnIndex, float spawnVariance, EnemyMovementType movementType)
        {
            switch (type)
            {
                case EnemyType.Basic:
                    _currentEnemy = PoolManager.Instance.GetFreeObject(BasicEnemyParent, BasicEnemyPrefab);
                    break;

                case EnemyType.NormalShot:
                    _currentEnemy = PoolManager.Instance.GetFreeObject(NormalShotParent, NormalShotPrefab);
                    break;

                case EnemyType.CurveShot:
                    _currentEnemy = PoolManager.Instance.GetFreeObject(CurveShotParent, CurveShotPrefab);
                    break;

                case EnemyType.DoubleShot:
                    _currentEnemy = PoolManager.Instance.GetFreeObject(DoubleShotParent, DoubleShotPrefab);
                    break;

                case EnemyType.TripleShot:
                    _currentEnemy = PoolManager.Instance.GetFreeObject(TripleShotParent, TripleShotPrefab);
                    break;

                case EnemyType.Miniboss1:
                    _currentEnemy = Miniboss1;
                    break;

                case EnemyType.Boss1:
                    _currentEnemy = Boss1;
                    break;
            }

            _currentEnemy.transform.position = SpawnPoints[spawnIndex].position + Vector3.right * spawnVariance;
            _currentEnemy.transform.rotation = SpawnPoints[spawnIndex].rotation;

            _currentEnemy.SetActive(true);
            _currentEnemy.GetComponent<BasicEnemy>().Revive(movementType);

            WaveManager.Instance.AddToAliveList(_currentEnemy);
        }
    }
}