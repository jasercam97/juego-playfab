﻿public enum SpawnPositions
{
    Left, MidLeft, Center, MidRight, Right, LeftLateral, RightLateral
}
