﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Enemies
{
    public class EnemyMovementController : MonoBehaviour
    {
        private EnemyMovementType _movementType;
        private float _moveSpeed;

        // Stop
        private float _stopDelay = 0.75f;
        private float _stopDuration = 1f;

        // Loop
        private float _loopDelay = 2f;
        private float _loopDuration = 3f;

        // Diagonal
        private float _diagonalDelay = 0.5f;
        private float _diagonalDuration = 0.75f;

        // Left Right
        private float _leftRightAmount = 2f;
        private float _leftRightDuration = 5f;

        // Quarter
        private float _quarterDelay = 0.25f;
        private float _quarterDuration = 2.5f;

        private Sequence _currentSequence;

        public void SetMoveSpeed(float moveSpeed)
        {
            _moveSpeed = moveSpeed;
        }

        public void SetMovementType(EnemyMovementType type)
        {
            _movementType = type;
        }

        private void Update()
        {
            transform.Translate(Vector2.up * _moveSpeed * Time.deltaTime);
        }

        public void DoPirouette()
        {
            switch (_movementType)
            {
                case EnemyMovementType.None:
                    DoStop();
                    break;

                case EnemyMovementType.Linear:
                    break;

                case EnemyMovementType.LoopRight:
                    DoLoopRight();
                    break;

                case EnemyMovementType.LoopLeft:
                    DoLoopLeft();
                    break;

                case EnemyMovementType.StopAndGo:
                    DoStopAndGo();
                    break;

                case EnemyMovementType.DiagonalRight:
                    DoDiagonalRight();
                    break;

                case EnemyMovementType.DiagonalLeft:
                    DoDiagonalLeft();
                    break;

                case EnemyMovementType.LeftRight:
                    DoStop();
                    DoLeftRight();
                    break;

                case EnemyMovementType.QuarterLeft:
                    DoQuarterLeft();
                    break;

                case EnemyMovementType.QuarterRight:
                    DoQuarterRight();
                    break;
            }
        }

        private void DoStop()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(DOTween.To(() => _moveSpeed, x => _moveSpeed = x, 0, _stopDuration)
                                           .SetDelay(_stopDelay)
                                           .SetEase(Ease.OutQuad));

            _currentSequence.Play();
        }

        private void DoLoopRight()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, 360), _loopDuration)
                                             .SetRelative()
                                             .SetDelay(_loopDelay)
                                             .SetEase(Ease.Linear));

            _currentSequence.Play();
        }

        private void DoLoopLeft()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, -360), _loopDuration)
                                             .SetRelative()
                                             .SetDelay(_loopDelay)
                                             .SetEase(Ease.Linear));

            _currentSequence.Play();
        }

        private void DoStopAndGo()
        {
        
        }

        private void DoDiagonalRight()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, 45), _diagonalDuration)
                                             .SetDelay(_diagonalDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, -45), _diagonalDuration)
                                             .SetDelay(_diagonalDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Play();
        }

        private void DoDiagonalLeft()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, -45), _diagonalDuration)
                                             .SetDelay(_diagonalDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, 45), _diagonalDuration)
                                             .SetDelay(_diagonalDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Play();
        }

        private void DoLeftRight()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DOMoveX(_leftRightAmount, _leftRightDuration)
                            .SetRelative()
                            .SetEase(Ease.InOutQuad));

            _currentSequence.Append(transform.DOMoveX(-_leftRightAmount, _leftRightDuration)
                            .SetRelative()
                            .SetEase(Ease.InOutQuad));

            _currentSequence.SetLoops(-1).Play();
        }

        private void DoQuarterRight()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, -90), _quarterDuration)
                                             .SetDelay(_quarterDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Play();
        }

        private void DoQuarterLeft()
        {
            _currentSequence = DOTween.Sequence();

            _currentSequence.Append(transform.DORotate(new Vector3(0, 0, 90), _quarterDuration)
                                             .SetDelay(_quarterDelay)
                                             .SetRelative()
                                             .SetEase(Ease.InOutQuad));

            _currentSequence.Play();
        }

        public void KillSequence()
        {
            _currentSequence.Kill();
        }
    }
}