﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Waves
{
    [System.Serializable]
    public class BurstData
    {
        public float TimeToReach;
        public BurstPattern Pattern;
        public SpawnPositions SpawnPosition;
        public BurstEnemies Enemies;
        public EnemyMovementType MovementType;
    }
}