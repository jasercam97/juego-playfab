﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Waves
{
    [CreateAssetMenu(fileName = "BurstEnemies", menuName = "ScriptableObjects/BurstEnemies")]
    public class BurstEnemies : ScriptableObject
    {
        public List<EnemyType> EnemyList;
    }
}