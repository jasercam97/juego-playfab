﻿public enum BurstPattern
{
    Single, Vertical, Horizontal, Triangle, Diamond, DiagonalLeftRight, DiagonalRightLeft
}