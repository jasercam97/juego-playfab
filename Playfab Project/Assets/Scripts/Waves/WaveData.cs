﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Waves
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObjects/Wave")]
    public class WaveData : ScriptableObject
    {
        public string Label;
        public List<BurstData> BurstList;

        public float GetBurstTimeToReach(int burstIndex)
        {
            return BurstList[burstIndex].TimeToReach;
        }
    }
}