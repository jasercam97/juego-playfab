﻿using Audio;
using Control;
using Menu;
using PlayerData;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using Utils;

namespace Waves
{
    public class WaveManager : Singleton<WaveManager>
    {
        public int StageIndex;
        public bool IsBossWave;
        public List<WaveData> WaveList;
        public int MoneyReward { get; private set; }
        public int ExpReward { get; private set; }

        private WaveData _currentWaveData;

        private int _currentWaveIndex;
        private int _currentBurstIndex;

        private List<GameObject> _currentlyAliveEnemies;

        private bool _playerDead;

        private void Awake()
        {
            _currentlyAliveEnemies = new List<GameObject>();
        }

        private void Start()
        {
            StartLevel();
        }

        private void StartLevel()
        {
            if (IsBossWave)
            {
                AudioManager.Instance.TransitionToBossMusic();
            }
            else
            {
                AudioManager.Instance.TransitionToGameMusic();
            }

            _currentWaveIndex = 0;
            StartWave();
        }

        private void StartWave()
        {
            _currentBurstIndex = 0;
            _currentWaveData = WaveList[_currentWaveIndex];

            Invoke(nameof(DoBurst), _currentWaveData.GetBurstTimeToReach(_currentBurstIndex));
        }

        private void DoBurst()
        {
            BurstManager.Instance.DoBurst(_currentWaveData.BurstList[_currentBurstIndex]);
            _currentBurstIndex++;

            if (_currentBurstIndex < _currentWaveData.BurstList.Count)
            {
                Invoke(nameof(DoBurst), _currentWaveData.GetBurstTimeToReach(_currentBurstIndex));
            }
        }

        public void AddToAliveList(GameObject enemy)
        {
            _currentlyAliveEnemies.Add(enemy);
        }

        public void RemoveFromAliveList(GameObject enemy)
        {
            _currentlyAliveEnemies.Remove(enemy);

            if (_currentBurstIndex == _currentWaveData.BurstList.Count && _currentlyAliveEnemies.Count == 0 && !_playerDead)
            {
                EndWave();
            }
        }

        private void EndWave()
        {
            _currentWaveIndex++;

            if (_currentWaveIndex == WaveList.Count)
            {
                EndLevel();
            }
        }

        public void StopWave()
        {
            _playerDead = true;
            CancelInvoke(nameof(DoBurst));
            CancelInvoke(nameof(ShowWinPanel));

            EndLevel();
        }

        private void EndLevel()
        {
            if (!_playerDead)
            {
                if (StageManager.Instance.CurrentStage == StageIndex)
                {
                    StageManager.Instance.CurrentStage++;
                }

                AudioManager.Instance.PlayStageCompleteSound();
                Invoke(nameof(ShowWinPanel), 1);
            }
            else
            {
                Invoke(nameof(ShowLosePanel), 1);
            }

            LevelManager.Instance.IncreaseExp(ExpReward);
            EconomyManager.Instance.CurrentMoney += MoneyReward;

            GameManager.Instance.UpdateDataAfterWave();
        }

        public void AddToRewards(int exp, int money)
        {
            ExpReward += exp;
            MoneyReward += money;
        }

        private void ShowWinPanel()
        {
            GameUIController.Instance.ShowEndPanel(true);
        }

        private void ShowLosePanel()
        {
            GameUIController.Instance.ShowEndPanel(false);
        }
    }
}