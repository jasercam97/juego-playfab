﻿using Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Waves
{
    public class BurstManager : Singleton<BurstManager>
    {
        public float SpawnDelay;
        public float MaxVariance;
        public float MinDistance;

        private float _spawnVariance;

        public void DoBurst(BurstData burstData)
        {
            _spawnVariance = Random.Range(-MaxVariance, MaxVariance);

            switch (burstData.Pattern)
            {
                case BurstPattern.Vertical:
                    StartCoroutine(DoVerticalBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, burstData.MovementType));
                    break;

                case BurstPattern.Horizontal:
                    DoHorizontalBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, burstData.MovementType);
                    break;

                case BurstPattern.Triangle:
                    StartCoroutine(DoTriangleBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, burstData.MovementType));
                    break;

                case BurstPattern.Diamond:
                    StartCoroutine(DoDiamondBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, burstData.MovementType));
                    break;

                case BurstPattern.DiagonalLeftRight:
                    StartCoroutine(DoDiagonalBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, true, burstData.MovementType));
                    break;

                case BurstPattern.DiagonalRightLeft:
                    StartCoroutine(DoDiagonalBurst(burstData.Enemies.EnemyList, burstData.SpawnPosition, false, burstData.MovementType));
                    break;

                case BurstPattern.Single:
                    DoSingleBurst(burstData.Enemies.EnemyList[0], burstData.SpawnPosition, burstData.MovementType);
                    break;

                default:
                    DoSingleBurst(burstData.Enemies.EnemyList[0], burstData.SpawnPosition, burstData.MovementType);
                    break;
            }
        }

        private IEnumerator DoVerticalBurst(List<EnemyType> enemyList, SpawnPositions spawnPosition, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);

            int i = 0;

            while (i < enemyList.Count)
            {
                SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance, movementType);

                yield return new WaitForSeconds(SpawnDelay);

                i++;
            }
        }

        private IEnumerator DoDiagonalBurst(List<EnemyType> enemyList, SpawnPositions spawnPosition, bool leftRight, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);

            int i = 0;

            while (i < enemyList.Count)
            {
                if (leftRight)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance + i * MinDistance, movementType);
                }
                else
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance - i * MinDistance, movementType);
                }

                yield return new WaitForSeconds(SpawnDelay);

                i++;
            }
        }

        private void DoHorizontalBurst(List<EnemyType> enemyList, SpawnPositions spawnPosition, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);
            float halfPoint = (enemyList.Count - 1) / 2f;

            for (int i = 0; i < enemyList.Count; i++)
            {
                if (i < halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance - MinDistance * (halfPoint - i), movementType);
                }
                else if (i > halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance + MinDistance * (i - halfPoint), movementType);
                }
                else
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance, movementType);
                }
            }
        }

        private void DoSingleBurst(EnemyType enemy, SpawnPositions spawnPosition, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);

            SpawnEnemy(enemy, spawnIndex, _spawnVariance, movementType);
        }

        private IEnumerator DoTriangleBurst(List<EnemyType> enemyList, SpawnPositions spawnPosition, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);

            int maxIterations = 1;
            int currentIteration = 0;
            float halfPoint = 0;

            int i = 0;

            while (i < enemyList.Count)
            {
                if (currentIteration < halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance - MinDistance * (halfPoint - currentIteration), movementType);
                }
                else if (currentIteration > halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance + MinDistance * (currentIteration - halfPoint), movementType);
                }
                else
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance, movementType);
                }
            
                i++;
                currentIteration++;

                if (currentIteration == maxIterations)
                {
                    currentIteration = 0;
                    maxIterations++;
                    halfPoint = (maxIterations - 1) / 2f;

                    yield return new WaitForSeconds(SpawnDelay);
                }
            }
        }

        private IEnumerator DoDiamondBurst(List<EnemyType> enemyList, SpawnPositions spawnPosition, EnemyMovementType movementType)
        {
            int spawnIndex = GetSpawnIndex(spawnPosition);

            int maxIterations = 1;
            int currentIteration = 0;
            float halfPoint = 0;

            bool increasingIterations = true;

            int i = 0;

            while (i < enemyList.Count)
            {
                if (currentIteration < halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance - MinDistance * (halfPoint - currentIteration), movementType);
                }
                else if (currentIteration > halfPoint)
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance + MinDistance * (currentIteration - halfPoint), movementType);
                }
                else
                {
                    SpawnEnemy(enemyList[i], spawnIndex, _spawnVariance, movementType);
                }

                i++;
                currentIteration++;

                if (currentIteration == maxIterations)
                {
                    currentIteration = 0;

                    if (maxIterations >= Mathf.Sqrt(enemyList.Count))
                    {
                        increasingIterations = false;
                    }

                    if (increasingIterations)
                    {
                        maxIterations++;
                    }
                    else
                    {
                        maxIterations--;
                    }

                    halfPoint = (maxIterations - 1) / 2f;

                    yield return new WaitForSeconds(SpawnDelay);
                }
            }
        }

        private void SpawnEnemy(EnemyType enemyType, int spawnIndex, float spawnVariance, EnemyMovementType movementType)
        {
            EnemySpawner.Instance.SpawnEnemy(enemyType, spawnIndex, spawnVariance, movementType);
        }


        private int GetSpawnIndex(SpawnPositions enumValue)
        {
            switch (enumValue)
            {
                case SpawnPositions.Left:
                    return 0;

                case SpawnPositions.MidLeft:
                    return 1;

                case SpawnPositions.Center:
                    return 2;

                case SpawnPositions.MidRight:
                    return 3;

                case SpawnPositions.Right:
                    return 4;

                case SpawnPositions.LeftLateral:
                    return 5;

                case SpawnPositions.RightLateral:
                    return 6;

                default:
                    return 0;
            }
        }
    }
}