﻿using Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class UIButtonSounds : MonoBehaviour
    {
        public bool IsConfirmButton;
        private Button UIButton;

        private void Awake()
        {
            UIButton = GetComponent<Button>();
            UIButton.onClick.AddListener(DoClickSound);
        }

        private void DoClickSound()
        {
            if (IsConfirmButton)
            {
                AudioManager.Instance.PlayConfirmSound();
            }
            else
            {
                AudioManager.Instance.PlayButtonClickSound();
            }
        }
    }
}