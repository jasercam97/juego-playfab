﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using TMPro;
using UnityEngine.UI;

namespace UI
{
    public class StartUIController : Singleton<StartUIController>
    {
        public Button StartButton;
        public TMP_Text StartButtonText;
        public TMP_Text ConnectionStatusText;

        public void EnableStartButton()
        {
            StartButton.interactable = true;
            StartButtonText.text = "Play";
        }

        public void SetConnectionStatusText(string connectionStatus, bool isError)
        {
            ConnectionStatusText.text = connectionStatus;

            if (isError)
            {
                ConnectionStatusText.color = Color.yellow;
                StartButtonText.text = "Connection failed.";
            }
        }
    }
}