﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Menu;
using Control;

namespace UI
{
    public class FadeTransition : MonoBehaviour
    {
        public CanvasGroup FadePanel;
        public float FadeTime;

        private bool _fadeStarted;

        private void Start()
        {
            FadeOutTransition().Play();
            _fadeStarted = false;
        }

        private Tween FadeInTransition()
        {
            return FadePanel.DOFade(1, FadeTime)
                            .SetEase(Ease.InQuad);
        }

        private Tween FadeOutTransition()
        {
            return FadePanel.DOFade(0, FadeTime)
                            .SetEase(Ease.OutQuad);
        }

        public void LoadMenuTransition()
        {
            if (!_fadeStarted)
            {
                ActivatePanel();

                FadeInTransition().OnComplete(() => SceneController.Instance.LoadMenu())
                                  .Play();
            }
        }

        public void LoadStageTransition()
        {
            if (!_fadeStarted)
            {
                ActivatePanel();

                FadeInTransition().OnComplete(() => MenuManager.Instance.DoPlay())
                                  .Play();
            }
        }

        public void ReloadStageTransition()
        {
            if (!_fadeStarted)
            {
                ActivatePanel();

                FadeInTransition().OnComplete(() => SceneController.Instance.ReloadStage())
                                  .Play();
            }
        }

        public void LoadNextStageTransition()
        {
            if (!_fadeStarted)
            {
                ActivatePanel();

                FadeInTransition().OnComplete(() => SceneController.Instance.LoadNextStage())
                                  .Play();
            }
        }

        private void ActivatePanel()
        {
            _fadeStarted = true;
            FadePanel.interactable = true;
            FadePanel.blocksRaycasts = true;
        }
    }
}