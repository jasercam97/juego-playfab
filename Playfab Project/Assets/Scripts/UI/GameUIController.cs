﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using Menu;
using PlayerData;
using Player;
using Audio;
using Control;
using Waves;

namespace UI
{
    public class GameUIController : Singleton<GameUIController>
    {
        [Header("HUD")]
        public float UpdateTime;

        [Space(5)]
        public Slider HealthBar;
        public Image HealthBarFill;
        public Color MaxHealthColor;
        public Color MinHealthColor;

        [Space(5)]
        public Slider ActiveBar;
        public GameObject DoubleTapText;

        [Header("End Panel")]
        public RectTransform EndPanel;
        public RectTransform OutsideScreenPoint;
        public GameObject NextLevelButton;
        public TMP_Text EndTitleText;
        public float TimeToShow;
        public float EffectsDuration;

        [Header("Exp Panel")]
        public TMP_Text LevelText;
        public Slider ExpBar;
        public float ExpBarGrowAmount;
        public float ExpBarGrowDuration;
        public AnimationCurve ExpBarGrowCurve;
        public GameObject NewLevelPanel;
        public TMP_Text NewHealthText;
        public TMP_Text NewDamageText;

        private int _initialLevel;

        [Header("Money Panel")]
        public TMP_Text MoneyText;

        private int _initialMoney;


        private void Start()
        {
            InitializeUI();
        }

        private void InitializeUI()
        {
            HealthBar.value = 1;
            HealthBarFill.color = MaxHealthColor;
            ActiveBar.value = 0;

            _initialLevel = LevelManager.Instance.CurrentLevel;
            _initialMoney = EconomyManager.Instance.CurrentMoney;

            LevelText.text = _initialLevel.ToString();
            ExpBar.value = LevelManager.Instance.GetExpPercent();
            MoneyText.text = _initialMoney.ToString();
        }

        public void UpdateHUDHealth(float healthPercent)
        {
            HealthBar.DOValue(healthPercent, UpdateTime)
                     .SetEase(Ease.OutQuad)
                     .Play();

            HealthBarFill.DOColor(Color.Lerp(MinHealthColor, MaxHealthColor, healthPercent), UpdateTime)
                         .SetEase(Ease.OutQuad)
                         .Play();
        }

        public void UpdateHUDActive(float activePercent)
        {
            ActiveBar.DOValue(activePercent, UpdateTime)
                     .SetEase(Ease.OutQuad)
                     .Play();

            if (activePercent >= 1)
            {
                DoubleTapText.SetActive(true);
            }
            else
            {
                DoubleTapText.SetActive(false);
            }
        }

        public void ShowEndPanel(bool hasWon)
        {
            if (hasWon)
            {
                EndTitleText.text = "STAGE COMPLETE!";

                if (WaveManager.Instance.StageIndex == StageManager.Instance.TotalStages)
                {
                    NextLevelButton.SetActive(false);
                }
            }
            else
            {
                EndTitleText.text = "YOU LOST...";
                NextLevelButton.gameObject.SetActive(false);
            }

            EndPanel.gameObject.SetActive(true);

            EndPanel.DOMoveY(EndPanel.position.y, TimeToShow)
                    .From(OutsideScreenPoint.position.y)
                    .SetEase(Ease.OutQuad)
                    .OnComplete(() => DoEffects())
                    .Play();
        }

        public void LoadMenu()
        {
            SceneController.Instance.LoadMenu();
        }

        public void TryAgain()
        {
            SceneController.Instance.ReloadStage();
        }

        public void NextStage()
        {
            SceneController.Instance.LoadNextStage();
        }

        private void DoEffects()
        {
            DoExpEffect();
            DoMoneyEarnEffect();
        }

        private void DoExpEffect()
        {
            if (LevelManager.Instance.CurrentLevel > _initialLevel)
            {
                ExpBarIncreaseTween(1).OnComplete(() => DoLevelUpEffect())
                                  .Play();
            }
            else
            {
                if (LevelManager.Instance.IsMaxLevel)
                {
                    ExpBar.value = 1;
                }
                else
                {
                    ExpBarIncreaseTween(LevelManager.Instance.GetExpPercent()).Play();
                }
            }
        }

        private void DoLevelUpEffect()
        {
            AudioManager.Instance.PlayLevelUpSound();

            ExpBar.transform.DOScale(transform.localScale * ExpBarGrowAmount, ExpBarGrowDuration)
                            .SetEase(ExpBarGrowCurve)
                            .OnComplete(() => ExpBarIncreaseTween(LevelManager.Instance.GetExpPercent()).Play())
                            .Play();
          
            int currentLevel = LevelManager.Instance.CurrentLevel;
            PlayerStats stats = PlayerStatsDatabase.Instance.GetStatsByLevel(currentLevel);

            LevelText.text = currentLevel.ToString();
            NewHealthText.text = "New health: " + stats.MaxHealth;
            NewDamageText.text = "New damage: " + stats.Damage;

            NewLevelPanel.SetActive(true);
            NewLevelPanel.transform.DOScaleY(1, ExpBarGrowDuration)
                                   .From(Vector2.zero)
                                   .Play();
        }

        private Tween ExpBarIncreaseTween(float endValue)
        {
            return ExpBar.DOValue(endValue, EffectsDuration)
                         .SetEase(Ease.InOutQuad);
        }

        private void DoMoneyEarnEffect()
        {
            int endValue = EconomyManager.Instance.CurrentMoney;
            int currentValue = _initialMoney;

            DOTween.To(() => currentValue, x => currentValue = x, endValue, EffectsDuration)
                   .SetEase(Ease.OutQuad)
                   .OnUpdate(() => MoneyText.text = currentValue.ToString())
                   .OnComplete(() => MoneyText.text = currentValue.ToString());
        }
    }
}