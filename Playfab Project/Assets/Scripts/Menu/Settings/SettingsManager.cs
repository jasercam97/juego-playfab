﻿using Audio;
using Control;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Menu
{
    public class SettingsManager : MonoBehaviour
    {
        public TMP_Text VersionText;
        public TMP_Text SoundButtonText;
        public TMP_Text MusicButtonText;

        private bool _soundActive;
        private bool _musicActive;

        private void Start()
        {
            VersionText.text = "Version " + GameManager.Instance.GameVersion;

            _soundActive = PlayerPrefs.GetInt(AudioManager.KEY_SOUND_ACTIVE, 1) == 1;
            _musicActive = PlayerPrefs.GetInt(AudioManager.KEY_MUSIC_ACTIVE, 1) == 1;

            SoundButtonText.text = _soundActive ? "SOUND ON" : "SOUND OFF";
            MusicButtonText.text = _musicActive ? "MUSIC ON" : "MUSIC OFF";
        }

        public void ToggleSound()
        {
            _soundActive = !_soundActive;

            AudioManager.Instance.SetSoundEnabled(_soundActive);
            SoundButtonText.text = _soundActive ? "SOUND ON" : "SOUND OFF";
        }

        public void ToggleMusic()
        {
            _musicActive = !_musicActive;

            AudioManager.Instance.SetMusicEnabled(_musicActive);
            MusicButtonText.text = _musicActive ? "MUSIC ON" : "MUSIC OFF";
        }
    }
}