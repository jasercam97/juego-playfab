﻿using UnityEngine.UI;

namespace Menu
{
    [System.Serializable]
    public class Level
    {
        public string ID;
        public bool Unlocked;
        public int SceneIndex;
        public Image LevelImage;
    }
}
