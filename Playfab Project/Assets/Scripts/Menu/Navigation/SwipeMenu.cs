﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class SwipeMenu : MonoBehaviour
    {
        public Scrollbar Scroll;

        public Button ShopButton;
        public Button PlayButton;
        public Button BuildButton;

        private float _scrollPos = 0.5f;
        private float[] _pos;
        private float _distance;

        private bool _isScrolling = false;
        private float _timer;

        private bool _checkButtons;

        private void Start()
        {
            _pos = new float[transform.childCount];

            _distance = 1f / (_pos.Length - 1f);

            for (int i = 0; i < _pos.Length; i++)
            {
                _pos[i] = _distance * i;
            }

            CheckButtons(1); //1 es el indice del panel PLAY
        }

        private void Update()
        {
            if(Input.GetMouseButton(0))
            {
                _isScrolling = true;
                _checkButtons = true;
            }
            else if(_isScrolling)
            {
                _timer += Time.deltaTime;

                if (_timer >= 0.15f)
                {
                    _scrollPos = Scroll.value;
                    _isScrolling = false;
                    _timer = 0;
                }
            }
            else 
            {
                for (int i = 0; i < _pos.Length; i++)
                {
                    if (_scrollPos < _pos[i] + (_distance / 2) && _scrollPos > _pos[i] - (_distance / 2))
                    {
                        Scroll.value = Mathf.Lerp(Scroll.value, _pos[i], 0.1f);
                        if (_checkButtons) { CheckButtons(i); }
                        break;
                    }
                }
            }

        }

        public void GoToDesiredPanel(int index)
        {
            _scrollPos = _pos[index];
            _isScrolling = false;

            CheckButtons(index);
        }

        void CheckButtons(int index)
        {
            _checkButtons = false;

            switch (index)
            {
                case 0: // Shop
                    ShopButton.interactable = false;
                    PlayButton.interactable = true;
                    BuildButton.interactable = true;

                    break;

                case 1: // Play
                    PlayButton.interactable = false;
                    ShopButton.interactable = true;
                    BuildButton.interactable = true;

                    break;

                case 2: // Build
                    MenuManager.Instance.HideTutorialText();
                    BuildButton.interactable = false;
                    ShopButton.interactable = true;
                    PlayButton.interactable = true;

                    break;

                default:
                    break;
            }
        }
    }
}