﻿using Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Audio;
using Control;

namespace Menu
{
    public class EquipmentSelector : MonoBehaviour
    {
        public enum TypeOfEquipment { Shoot, Defense, Active }
        public TypeOfEquipment EquipType;

        public Scrollbar Scroll;

        public TMP_Text ItemText;

        public ItemType NormalShoot;
        public ItemType EnergyPulse;

        public float _scrollPos = 1f;
        private float[] _pos;
        private float _distance;

        private bool _isScrolling = false;
        private float _timer;

        private int index;

        private bool _equipmentChanged = false;
        private bool _isInitialized = false;

        private int id;

        private void Start()
        {
            StartingUnlocks();  // TODO Esto en teoría no debería hacer falta

            HideLockedItems();

            Invoke(nameof(SetInitialized), 0.75f);
        }

        void StartingUnlocks()
        {
            if (NormalShoot) 
            {
                NormalShoot.UnlockItem(true);
                NormalShoot.gameObject.SetActive(true);
            }

            if (EnergyPulse)
            {
                EnergyPulse.UnlockItem(true);
                EnergyPulse.gameObject.SetActive(true);
            }
        }

        private void HideLockedItems()
        {
            foreach (Transform child in transform)
            {
                if (!child.GetComponent<ItemType>().IsUnlocked())
                {
                    child.gameObject.SetActive(false);
                }
            }

            CalculateScroll();
        }

        public void CalculateScroll()
        {
            int count = 0;

            foreach (Transform child in transform)
            {
                if (child.gameObject.activeSelf) 
                {
                    count++; 
                    child.gameObject.GetComponent<ScrollID>().SetID(count);
                }
            }

            _pos = new float[count];

            _distance = 1f / (_pos.Length - 1f);

            for (int i = 0; i < _pos.Length; i++)
            {
                _pos[i] = _distance * i;

            }

            SetEquippedItems();
        }

        private void Update()
        {
            if (Input.GetMouseButton(0))
            {
                _isScrolling = true;
            }
            else if (_isScrolling)
            {
                _timer += Time.deltaTime;

                if (_timer >= 0.15f)
                {
                    _scrollPos = Scroll.value;
                    _isScrolling = false;
                    _timer = 0;
                }
            }
            else
            {
                for (int i = 0; i < _pos.Length; i++)
                {
                    if (_scrollPos < _pos[i] + (_distance / 2) && _scrollPos > _pos[i] - (_distance / 2))
                    {
                        Scroll.value = Mathf.Lerp(Scroll.value, _pos[i], 0.1f);
                        index = i;

                        if(Scroll.value < 0.1f) { Scroll.value = 0; }
                        if(Scroll.value > 0.9f) { Scroll.value = 1; }

                        break;
                    }
                }
            }
        }

        void SetEquippedItems()
        {
            if (_pos.Length <= 0) { return; }

            switch (EquipType)
            {
                case TypeOfEquipment.Shoot:
                    switch (ShipEquipment.Instance.ShootEquipped)
                    {
                        case ShootType.NormalShot:
                            id = transform.GetChild(0).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case ShootType.DoubleShot:
                            id = transform.GetChild(1).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case ShootType.TripleShot:
                            id = transform.GetChild(2).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case ShootType.CurveShot:
                            id = transform.GetChild(3).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;

                        default:
                            break;
                    }
                    break;

                case TypeOfEquipment.Defense:
                    switch (ShipEquipment.Instance.DefenseEquipped)
                    {
                        case DefenseType.FixedShield:
                            id = transform.GetChild(0).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case DefenseType.IntermittentShield:
                            id = transform.GetChild(1).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case DefenseType.RotativeShield:
                            id = transform.GetChild(2).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;

                        default:
                            break;
                    }
                    break;

                case TypeOfEquipment.Active:
                    switch (ShipEquipment.Instance.ActiveEquipped)
                    {
                        case ActiveType.EnergyPulse:
                            id = transform.GetChild(0).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case ActiveType.Invincibility:
                            id = transform.GetChild(1).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;
                        case ActiveType.FastShot:
                            id = transform.GetChild(2).GetComponent<ScrollID>().GettID();
                            _scrollPos = _pos[_pos.Length - id];
                            Scroll.value = _pos[_pos.Length - id];
                            break;

                        default:
                            break;
                    }

                    break;
            }

            EquipItem();
        }

        public void EquipItem()
        {
            if (_pos == null || _pos.Length <= 0) { return; }

            float dist = 0;
            ItemType item = null;

            foreach (Transform child in transform)
            {
                if (Vector2.Distance(child.position, transform.parent.position) < dist || dist == 0)
                {
                    if (child.gameObject.activeSelf)
                    {
                        dist = Vector2.Distance(child.position, transform.parent.position);
                        item = child.GetComponent<ItemType>();
                    }
                }
            }

            EquipmentType type = item.GetItemType();

            if (_isInitialized)
            {
                switch (type)
                {
                    case EquipmentType.NormalShot:
                        if (ShipEquipment.Instance.ShootEquipped != ShootType.NormalShot)
                        {
                            ShipEquipment.Instance.EquipShoot(ShootType.NormalShot);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.DoubleShot:
                        if (ShipEquipment.Instance.ShootEquipped != ShootType.DoubleShot)
                        {
                            ShipEquipment.Instance.EquipShoot(ShootType.DoubleShot);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.TripleShot:
                        if (ShipEquipment.Instance.ShootEquipped != ShootType.TripleShot)
                        {
                            ShipEquipment.Instance.EquipShoot(ShootType.TripleShot);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.CurveShot:
                        if (ShipEquipment.Instance.ShootEquipped != ShootType.CurveShot)
                        {
                            ShipEquipment.Instance.EquipShoot(ShootType.CurveShot);
                            _equipmentChanged = true;
                        }
                        break;

                    case EquipmentType.FixedShield:
                        if (ShipEquipment.Instance.DefenseEquipped != DefenseType.FixedShield)
                        {
                            ShipEquipment.Instance.EquipDefense(DefenseType.FixedShield);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.IntermittentShield:
                        if (ShipEquipment.Instance.DefenseEquipped != DefenseType.IntermittentShield)
                        {
                            ShipEquipment.Instance.EquipDefense(DefenseType.IntermittentShield);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.RotativeShield:
                        if (ShipEquipment.Instance.DefenseEquipped != DefenseType.RotativeShield)
                        {
                            ShipEquipment.Instance.EquipDefense(DefenseType.RotativeShield);
                            _equipmentChanged = true;
                        }
                        break;

                    case EquipmentType.EnergyPulse:
                        if (ShipEquipment.Instance.ActiveEquipped != ActiveType.EnergyPulse)
                        {
                            ShipEquipment.Instance.EquipActive(ActiveType.EnergyPulse);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.Invincibility:
                        if (ShipEquipment.Instance.ActiveEquipped != ActiveType.Invincibility)
                        {
                            ShipEquipment.Instance.EquipActive(ActiveType.Invincibility);
                            _equipmentChanged = true;
                        }
                        break;
                    case EquipmentType.FastShot:
                        if (ShipEquipment.Instance.ActiveEquipped != ActiveType.FastShot)
                        {
                            ShipEquipment.Instance.EquipActive(ActiveType.FastShot);
                            _equipmentChanged = true;
                        }
                        break;
                }

                if (_equipmentChanged)
                {
                    AudioManager.Instance.PlayEquipSound();

                    _equipmentChanged = false;
                    CancelInvoke(nameof(DoPlayfabUpdate));
                    Invoke(nameof(DoPlayfabUpdate), .75f);
                }
            }
            ItemText.text = item.GetName();
        }

        private void DoPlayfabUpdate()
        {
            GameManager.Instance.UpdateDataAfterEquip();
        }

        // Para que no haga el sonido de equipar al inicializarse
        private void SetInitialized()
        {
            _isInitialized = true;
        }
    }
}