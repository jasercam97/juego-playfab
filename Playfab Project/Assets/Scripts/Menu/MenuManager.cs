﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;
using Utils;
using System;
using PlayerData;
using Player;
using Control;
using Audio;

namespace Menu
{
    public class MenuManager : Singleton<MenuManager>
    {
        [Header("Resources")]
        public TMP_Text GoldText;
        public TMP_Text MoneyText;
        public TMP_Text LevelText;
        public Slider ExpBar;

        [Header("Levels")]
        public List<Level> ListOfLevels;
        private Level _levelSelected;

        [Header("Play Screen")]
        public GameObject LevelSelectedPanel;
        public TMP_Text LevelSelectedText;
        public Button PlayButton;
        public GameObject LockImage;

        [Header("Build Screen")]
        public TMP_Text HealthText;
        public TMP_Text DamageText;

        [Header("Settings Screen")]
        public GameObject SettingsPanel;
        public GameObject CodePanel;
        public GameObject CreditsPanel;

        [Header("Tutorial text")]
        public CanvasGroup TutorialCanvas;
        private bool _tutorialShown = false;

        private void Start()
        {
            AudioManager.Instance.TransitionToMenuMusic();

            InitResources();
            InitLevelList();
            InitStats();

            //ShopManager.Instance.SetUnlockedItems();
        }

        private void InitResources()
        {
            SetMoneyText();
            SetLevelBar();
        }

        private void InitLevelList()
        {
            int maxStage = StageManager.Instance.CurrentStage;

            if (maxStage > StageManager.Instance.TotalStages)
            {
                maxStage = StageManager.Instance.TotalStages;
            }

            for (int i=0; i < ListOfLevels.Count; i++)
            {
                if (maxStage > i)
                {
                    ListOfLevels[i].Unlocked = true;
                }
                else
                {
                    ListOfLevels[i].LevelImage.color = Color.gray;
                }
            }

            SelectLevel(maxStage);
        }

        private void InitStats()
        {
            PlayerStats stats = PlayerStatsDatabase.Instance.StatsPerLevel[LevelManager.Instance.CurrentLevel];

            HealthText.text = "Health: " + stats.MaxHealth;
            DamageText.text = "Damage: " + stats.Damage;
        }

        public void SetMoneyText()
        {
            GoldText.text = "0";

            int endValue = EconomyManager.Instance.CurrentMoney;
            int currentValue = int.Parse(MoneyText.text);

            DOTween.To(() => currentValue, x => currentValue = x, endValue, 0.5f)
                   .SetEase(Ease.OutQuad)
                   .OnUpdate(() => MoneyText.text = currentValue.ToString())
                   .OnComplete(() => MoneyText.text = currentValue.ToString());
        }

        private void SetLevelBar()
        {
            LevelText.text = LevelManager.Instance.CurrentLevel.ToString();
            ExpBar.value = LevelManager.Instance.GetExpPercent();
        }

        public void SelectLevel(int level)
        {
            ShowPopup(LevelSelectedPanel, 0.5f);

            _levelSelected = ListOfLevels[level - 1];
            LevelSelectedText.text = _levelSelected.ID;

            PlayButton.interactable = _levelSelected.Unlocked;
            LockImage.SetActive(!_levelSelected.Unlocked);
        }  

        public void DoPlay()
        {
            SceneController.Instance.LoadStage(_levelSelected.SceneIndex);
        }

        #region SETTINGS

        public void ShowSettings(float time)
        {
            Debug.Log("Settings");
            ShowPopup(SettingsPanel, time);
        }

        public void HideSettings(float time)
        {
            HidePopup(SettingsPanel, time);
        }

        public void ShowCodePanel(float time)
        {
            ShowPopup(CodePanel, time);
        }

        public void HideCodePanel(float time)
        {
            HidePopup(CodePanel, time);
        }
        public void ShowCreditsPanel(float time)
        {
            ShowPopup(CreditsPanel, time);
        }

        public void HideCreditsPanel(float time)
        {
            HidePopup(CreditsPanel, time);
        }

        #endregion

        void ShowPopup(GameObject GO, float time)
        {
            GO.transform.localScale = Vector3.zero;
            GO.transform.DOScale(1, time)
                .SetEase(Ease.OutBack)
                .Play();
        }

        void HidePopup(GameObject GO, float time)
        {
            GO.transform.DOScale(0, time)
                .SetEase(Ease.InBack)
                .Play();
        }

        public void ShowTutorialText()
        {
            if (PlayerPrefs.GetInt("TutorialSeen", 0) == 0)
            {
                _tutorialShown = true;
                
                TutorialCanvas.alpha = 1;
            }
        }

        public void HideTutorialText()
        {
            if (_tutorialShown)
            {
                _tutorialShown = false;
                PlayerPrefs.SetInt("TutorialSeen", 1);

                TutorialCanvas.DOFade(0, 1.5f)
                              .SetEase(Ease.InQuad)
                              .Play();
            }
        }
    }
}
