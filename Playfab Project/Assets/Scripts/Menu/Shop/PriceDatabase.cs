﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Menu
{
    [System.Serializable]
    public class PriceDatabase : Singleton<PriceDatabase>
    {
        public List<ItemData> ItemList;

        public void Initialize(List<ItemData> itemData)
        {
            ItemList.AddRange(itemData);

            foreach (ItemData item in ItemList)
            {
                item.Type = EnumUtils.Instance.FindEnumValue<EquipmentType>(item.Name);
            }
        }

        public ItemData GetItemData(EquipmentType equipmentType)
        {
            return ItemList.Find(item => item.Type == equipmentType);
        }
    }

    [System.Serializable]
    public class ItemData
    {
        public string Name;
        public EquipmentType Type;
        public int Cost;
        public int LevelToUnlock;
    }
}