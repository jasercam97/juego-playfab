﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using DG.Tweening;
using Player;
using Audio;
using PlayerData;
using Control;

namespace Menu {
    public class ShopItem : MonoBehaviour
    {
        [Header("Equipment Panel")]
        public ItemType EquipmentItem;

        public Item Item;

        public Image ItemSprite;

        public TMP_Text NameText;
        public TMP_Text CostText;
        public TMP_Text InfoText;

        public Button BuyButton;

        private bool _confirm;


        private void Awake()
        {
            InitItem();
        }

        private void InitItem()
        {

            Item.Cost = PriceDatabase.Instance.GetItemData(Item.Type).Cost;

            ItemSprite.sprite = Item.ItemSprite;
            NameText.text = Item.Name;
            CostText.text = Item.Cost.ToString() + " $";

            //ShopManager.Instance.AddItemToList(this);
            if (ShopManager.Instance.UnlockedEquipment.Contains(EquipmentItem.Type))
            {
                UnlockEquipment();
            }
        }

        public void BuyConfirmation()
        {
            if (!_confirm)
            {
                if (EconomyManager.Instance.CurrentMoney >= Item.Cost)
                {
                    AudioManager.Instance.PlayButtonClickSound();

                    BuyButton.GetComponentInChildren<TMP_Text>().text = "CONFIRM";
                    _confirm = true;
                    Invoke(nameof(CancelConfirmation), 10f);
                }
                else
                {
                    AudioManager.Instance.PlayCantBuySound();

                    transform.DOShakePosition(0.25f)
                        .SetEase(Ease.Linear)
                        .Play();
                }
            }
            else { BuyItem(); }
        }

        public EquipmentType GetEquipmentType()
        {
            return EquipmentItem.Type;
        }
        
        void BuyItem()
        {
            AudioManager.Instance.PlayBuySound();

            CancelInvoke((nameof(CancelConfirmation)));

            EconomyManager.Instance.UpdateMoney(-Item.Cost);

            ShopManager.Instance.AddUnlockedItem(EquipmentItem.Type);

            MenuManager.Instance.ShowTutorialText();

            UnlockEquipment();            
        }

        public void UnlockEquipment()
        {
            EquipmentItem.gameObject.SetActive(true);
            EquipmentItem.UnlockItem(true);

            CheckFirstShield();

            EquipmentItem.GetComponentInParent<EquipmentSelector>().CalculateScroll();


            ItemBought();
        }

        private void CheckFirstShield()
        {
            if(ShipEquipment.Instance.DefenseEquipped == DefenseType.None)
            {
                switch (EquipmentItem.Type)
                {
                    case EquipmentType.FixedShield:
                        ShipEquipment.Instance.EquipDefense(DefenseType.FixedShield);
                        break;
                    case EquipmentType.IntermittentShield:
                        ShipEquipment.Instance.EquipDefense(DefenseType.IntermittentShield);
                        break;
                    case EquipmentType.RotativeShield:
                        ShipEquipment.Instance.EquipDefense(DefenseType.RotativeShield);
                        break;

                    default:
                        break;
                }

                GameManager.Instance.UpdateDataAfterEquip();
            }
        }

        void ItemBought()
        {
            BuyButton.interactable = false;
            BuyButton.GetComponentInChildren<TMP_Text>().text = "BOUGHT";
        }

        void CancelConfirmation()
        {
            BuyButton.GetComponentInChildren<TMP_Text>().text = "BUY";
            _confirm = false;
        }
    }
}
