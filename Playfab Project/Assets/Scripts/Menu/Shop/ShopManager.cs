﻿using Control;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Menu
{
    public class ShopManager : Singleton<ShopManager>
    {
        public List<EquipmentType> UnlockedEquipment;

        public void InitializeUnlockedEquipment(List<string> unlockedList)
        {
            foreach (string i in unlockedList)
            {
                UnlockedEquipment.Add(EnumUtils.Instance.FindEnumValue<EquipmentType>(i));
            }
        }

        public void AddUnlockedItem(EquipmentType i)
        {
            if (!UnlockedEquipment.Contains(i))
            {
                UnlockedEquipment.Add(i);
                GameManager.Instance.UpdateDataAfterBuy(UnlockedEquipment);
            }
        }
    }
}
