﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu
{
    public class ScrollID : MonoBehaviour
    {
        private int ID;

        public void SetID(int i) { ID = i; }
        public int GettID() { return ID; }
    }
}