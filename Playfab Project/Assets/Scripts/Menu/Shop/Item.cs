﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu
{
    [CreateAssetMenu(fileName = "New Item", menuName = "ScriptableObjects/Shop Item")]
    public class Item : ScriptableObject
    {
        public string Name;
        public EquipmentType Type;
        public Sprite ItemSprite;
        public int Cost;
    }
}