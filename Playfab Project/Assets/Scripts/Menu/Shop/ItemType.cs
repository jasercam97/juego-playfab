﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu
{
    public class ItemType : MonoBehaviour
    {
        public string Name;
        public EquipmentType Type;

        private bool _unlocked = false;
        public EquipmentType GetItemType() { return Type; }

        public string GetName() { return Name; }

        public void UnlockItem(bool state) { _unlocked = state; }
        public bool IsUnlocked() { return _unlocked; }
    }
}