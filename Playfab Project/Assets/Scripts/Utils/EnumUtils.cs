﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Utils
{
    public class EnumUtils : Singleton<EnumUtils>
    {
        public bool Compare<T>(T enumValue, string stringToCompare)
        {
            string enumNormalized = Regex.Replace(enumValue.ToString(), @"\s", "");
            string stringNormalized = Regex.Replace(stringToCompare, @"\s", "");

            return string.Equals(enumNormalized, stringNormalized, System.StringComparison.OrdinalIgnoreCase);
        }

        public string FormattedEnum<T>(T enumValue)
        {
            return Regex.Replace(enumValue.ToString(), @"(\p{Lu})", " $1").Trim();
        }

        public T FindEnumValue<T>(string stringToFind)
        {
            foreach (T enumValue in System.Enum.GetValues(typeof(T)))
            {
                if (Compare(enumValue, stringToFind))
                {
                    return enumValue;
                }
            }

            return (T)System.Enum.GetValues(typeof(T)).GetValue(0);    // 0 needs to be a default value
        }
    }
}