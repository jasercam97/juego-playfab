﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bullets
{
    public class CurveBullet : MonoBehaviour
    {
        public float Speed;
        public float RotationRate;
        public float RotationAngle;

        private bool _turnRight;

        private float _currentAngle = 0;

        private void OnEnable()
        {
            //_turnRight = false;
            _currentAngle = 0;
        }

        private void Update()
        {
            transform.Translate(Vector2.up * Speed * Time.deltaTime);

            if (_turnRight)
            {
                transform.Rotate(0, 0, RotationRate * Time.deltaTime, Space.Self);
                _currentAngle += RotationRate * Time.deltaTime;
            }
            else
            {
                transform.Rotate(0, 0, -RotationRate * Time.deltaTime, Space.Self);
                _currentAngle -= RotationRate * Time.deltaTime;
            }         
            
            if(_currentAngle < -RotationAngle) { _turnRight = true; }
            if(_currentAngle > RotationAngle) { _turnRight = false; }            
        }

        public void SetStartDirection(bool turn)
        {
            _turnRight = turn;
        }
    }
}