﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bullets
{
    public class NormalBullet : MonoBehaviour
    {
        public float Speed;

        private void Update()
        {
            transform.Translate(Vector2.up * Speed * Time.deltaTime);
        }
    }
}