﻿using Audio;
using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bullets
{
    public class Bullet : MonoBehaviour
    {
        public NormalBullet NormalBulletBehaviour;
        public CurveBullet CurveBulletBehaviour;

        //public Transform ParticleParent;
        public ParticleSystem ParticlePrefab;

        public float Damage { get; set; }


        public void Initialize(float damage, Transform spawn, ShootType type)
        {
            Damage = damage;
            transform.position = spawn.position;
            transform.rotation = spawn.rotation;
            SetBehaviour(type);
        }

        private void SetBehaviour(ShootType type)
        {
            switch (type)
            {
                case ShootType.Default:
                    SetNormalBulletBehaviour();
                    break;

                case ShootType.NormalShot:
                    SetNormalBulletBehaviour();
                    break;

                case ShootType.DoubleShot:
                    SetNormalBulletBehaviour();
                    break;

                case ShootType.TripleShot:
                    SetNormalBulletBehaviour();
                    break;

                case ShootType.CurveShot:
                    SetCurvedBulletBehaviour();
                    break;
            }
        }

        private void SetNormalBulletBehaviour()
        {
            NormalBulletBehaviour.enabled = true;
            CurveBulletBehaviour.enabled = false;
        }

        private void SetCurvedBulletBehaviour()
        {
            NormalBulletBehaviour.enabled = false;
            CurveBulletBehaviour.enabled = true;
        }

        private void OnBecameInvisible()
        {
            gameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Instantiate(ParticlePrefab.gameObject, transform.position, transform.rotation);
            AudioManager.Instance.PlayBulletPopSound();
            gameObject.SetActive(false);
        }
    }
}