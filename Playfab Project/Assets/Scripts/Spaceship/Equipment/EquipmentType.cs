﻿public enum EquipmentType
{
    NormalShot, DoubleShot, TripleShot, CurveShot, FixedShield, IntermittentShield, RotativeShield, EnergyPulse, Invincibility, FastShot
}