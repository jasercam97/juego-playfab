﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Player
{
    public class ShipEquipment : Singleton<ShipEquipment>
    {
        public ShootType ShootEquipped;
        public DefenseType DefenseEquipped;
        public ActiveType ActiveEquipped;

        public void InitializeCurrentEquipment(List<string> equipmentList)
        {
            EquipShoot(EnumUtils.Instance.FindEnumValue<ShootType>(equipmentList[0]));      // 0 = Shoot
            EquipDefense(EnumUtils.Instance.FindEnumValue<DefenseType>(equipmentList[1]));  // 1 = Defense
            EquipActive(EnumUtils.Instance.FindEnumValue<ActiveType>(equipmentList[2]));    // 2 = Active
        }

        public void EquipShoot(ShootType newShoot)
        {
            ShootEquipped = newShoot;
        }

        public void EquipDefense(DefenseType newDefense)
        {
            DefenseEquipped = newDefense;
        }

        public void EquipActive(ActiveType newActive)
        {
            ActiveEquipped = newActive;
        }
    }
}