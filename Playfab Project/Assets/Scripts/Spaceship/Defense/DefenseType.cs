﻿public enum DefenseType
{
    None, FixedShield, IntermittentShield, RotativeShield
}