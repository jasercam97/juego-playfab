﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class ShieldDetector : MonoBehaviour
    {
        private Defense _defense;
        private bool _wasHit;

        private void Awake()
        {
            _defense = GameObject.FindObjectOfType<Defense>();
        }
      
        void ResetHitState() { _wasHit = false; }

        public bool CheckIfWasHit()
        {
            Invoke(nameof(ResetHitState), 0.5f);

            return _wasHit;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Bullet"))
            {
                collision.gameObject.SetActive(false);
                _wasHit = true;
                _defense.StopBullet();
            }
        }
    }
}