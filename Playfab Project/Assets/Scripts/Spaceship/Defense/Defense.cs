﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Defense : MonoBehaviour
    {
        private DefenseType _type;

        [Header("Fixed Defense")]
        public GameObject FixedShield;
        public int Health;
        private int _currentHealth;

        [Header("Intermittent Defense")]
        public GameObject IntermittentShield;
        public float DurationTime;
        public float TimeToReappear;

        [Header("Rotative Defense")]
        public GameObject RotativeShield;
        public float RotationSpeed;

        [Header("Audio")]
        public AudioSource DefenseAS;
        public AudioClip ShieldActivated;
        public AudioClip ShieldDeactivated;
        public AudioClip ShieldBroken;


        private GameObject _activeShield;

        private bool _isActive;

        private PlayerHealth _playerHealth;

        private void Awake()
        {
            _playerHealth = GetComponent<PlayerHealth>();
        }

        private void Start()
        {
            _isActive = true; //Quitar

            _type = ShipEquipment.Instance.DefenseEquipped;

            SetDefense();

            if (_activeShield != null)
            {
                _playerHealth.SetShieldDetector(_activeShield);
            }
        }


        private void SetDefense()
        {
            switch (_type)
            {
                case DefenseType.None:
                    //DisableDefense();
                    break;

                case DefenseType.FixedShield:
                    _currentHealth = Health;
                    _activeShield = FixedShield;
                    EnableDefense();
                    break;

                case DefenseType.IntermittentShield:
                    _activeShield = IntermittentShield;
                    StartCoroutine(nameof(IntermittentDefense));
                    break;

                case DefenseType.RotativeShield:
                    _activeShield = RotativeShield;
                    EnableDefense();
                    StartCoroutine(nameof(RotativeDefense));
                    break;

                default:
                    break;
            }
        }

        private IEnumerator RotativeDefense()
        {
            while (_isActive)
            {
                _activeShield.transform.Rotate(Vector3.forward, RotationSpeed * Time.deltaTime);
                yield return null;
            }
        }

        private IEnumerator IntermittentDefense()
        {
            
            while (_isActive)
            {
                yield return new WaitForSeconds(TimeToReappear);

                EnableDefense();
                yield return new WaitForSeconds(DurationTime);
                DisableDefense();
            }
        }

        public void StopBullet()
        {
            if(_type == DefenseType.FixedShield) { DamageShield(); }
        }

        [ContextMenu("Test DmgShield")]
        void DamageShield()
        {
            _currentHealth--;
            if(_currentHealth <= 0) 
            {
                DefenseAS.PlayOneShot(ShieldBroken);
                DisableDefense(); 
            }
        }

        void EnableDefense()
        {
            DefenseAS.PlayOneShot(ShieldActivated);

            _activeShield.SetActive(true);
        }

        private void DisableDefense()
        {
            DefenseAS.PlayOneShot(ShieldDeactivated);

            transform.GetComponentInChildren<ShieldEffect>()?.DisableShield();
        }
    }
}