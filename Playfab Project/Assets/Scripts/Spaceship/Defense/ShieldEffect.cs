﻿using UnityEngine;
using DG.Tweening;
using System;

namespace Player
{
    public class ShieldEffect : MonoBehaviour
    {

        private Vector3 _startScale;

        private void Awake()
        {
            _startScale = transform.localScale;
        }

        private void OnEnable()
        {
            transform.localScale = Vector3.zero;
            transform.DOScale(_startScale, 0.2f);
        }

        public void DisableShield()
        {
            transform.DOScale(Vector3.zero, 0.2f)
                .OnComplete(() => transform.parent.gameObject.SetActive(false));
        }
    }
}