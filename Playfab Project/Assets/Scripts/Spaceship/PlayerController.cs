﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player 
{
    public class PlayerController : MonoBehaviour
    {
        public float Speed;

        private float _deltaX;
        private float _deltaY;

        private Vector2 _screenLimits;

        private void Update()
        {
            //transform.position = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            //    Camera.main.ScreenToWorldPoint(Input.mousePosition).y);

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        _deltaX = touchPos.x - transform.position.x;
                        _deltaY = touchPos.y - transform.position.y;
                        break;

                    case TouchPhase.Moved:
                        transform.position = new Vector2(touchPos.x - _deltaX, touchPos.y - _deltaY);
                        break;
                }
            }
        }
    }
}