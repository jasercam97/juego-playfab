﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Audio;
using Waves;
using UI;
using PlayerData;
using Bullets;

namespace Player
{
    public class PlayerHealth : MonoBehaviour
    {
        public SpriteRenderer Sprite;
        public Color FlickerColor;
        public float InvulnerableTime;

        [Header("Particles")]
        public ParticleSystem DeathParticles;

        [Header("Sound")]
        public AudioSource HealthAS;
        public AudioClip GetHitSound;
        public AudioClip ShieldHitSound;

        private float _maxHealth;
        private float _currentHealth;
        private ShieldDetector _shieldDetector;
        private bool _canGetHit;
        private bool _isInvincible;

        private void Awake()
        {
            _maxHealth = PlayerStatsDatabase.Instance.GetStatsByLevel(LevelManager.Instance.CurrentLevel).MaxHealth;
        }

        private void Start()
        {
            _currentHealth = _maxHealth;
            _isInvincible = false;
            _canGetHit = true;
        }

        public void SetShieldDetector(GameObject detector)
        {
            _shieldDetector = detector.GetComponent<ShieldDetector>();
        }

        public void GetHit(float damage, bool isCollision)
        {
            if (_canGetHit || isCollision)
            {
                if (_shieldDetector != null && _shieldDetector.CheckIfWasHit() || _isInvincible)
                {
                    DoShieldHitSound();
                }
                else
                {
                    _canGetHit = false;
                    Sprite.color = FlickerColor;
                    Invoke(nameof(EnableGetHit), InvulnerableTime);

                    _currentHealth -= damage;
                    GameUIController.Instance.UpdateHUDHealth(_currentHealth / _maxHealth);

                    CheckHealth();
                }
            }
        }

        private void CheckHealth()
        {
            if (_currentHealth <= 0)
            { 
                Die();
            }
            else
            {
                DoGetHitSound();
                Camera.main.DOShakePosition(InvulnerableTime, 0.2f, 30).Play();
            }
        }

        private void Die()
        {
            Instantiate(DeathParticles, transform.position, transform.rotation);
            Camera.main.DOShakePosition(InvulnerableTime, 0.75f, 40).Play();
            AudioManager.Instance.PlayPlayerDeathSound();

            WaveManager.Instance.StopWave();
            gameObject.SetActive(false);
        }

        private void EnableGetHit()
        {
            _canGetHit = true;
            Sprite.color = Color.white;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Bullet"))
            {
                GetHit(collision.GetComponent<Bullet>().Damage, false);
            }
        }

        private void DoGetHitSound()
        {
            HealthAS.pitch = UnityEngine.Random.Range(0.95f, 1.05f);
            HealthAS.PlayOneShot(GetHitSound);
        }

        private void DoShieldHitSound()
        {
            HealthAS.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
            HealthAS.PlayOneShot(ShieldHitSound);
        }

        public void ToggleInvincibility()
        {
            _isInvincible = !_isInvincible;
        }
    }
}