﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Player
{
    public class PlayerStatsDatabase : Singleton<PlayerStatsDatabase>
    {
        public List<PlayerStats> StatsPerLevel;

        public void Initialize(List<PlayerStats> playerStats)
        {
            StatsPerLevel.AddRange(playerStats);
        }

        public PlayerStats GetStatsByLevel(int level)
        {
            return StatsPerLevel[level];
        }
    }

    [System.Serializable]
    public class PlayerStats
    {
        public int Level;
        public float MaxHealth;
        public float Damage;
    }
}