﻿using Bullets;
using PlayerData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Player
{
    public class Shoot : MonoBehaviour
    {
        public float Damage;

        [Range(0.05f, 1f)]
        public float ShootRate;
        private float _currentShootRate;

        private ShootType _type;

        [Header("Sound")]
        public AudioSource ShootAS;
        public AudioClip ShootSound;

        [Header("Normal Shoot")]
        public Transform NormalShootPosition;

        [Header("Double Shoot")]
        public List<Transform> DoubleShootPositions;

        [Header("Double Shoot")] 
        public List<Transform> TripleShootPositions;


        [Header("Bullet Pool")]
        public GameObject Prefab;
        public Transform Parent;

        public bool _canFire; //Hacer privado
        private GameObject _bullet;

        private bool _curveDir = false;

        private bool _isRuning;

        private void Awake()
        {
            Damage = PlayerStatsDatabase.Instance.GetStatsByLevel(LevelManager.Instance.CurrentLevel).Damage;
        }

        private void Start()
        {
            _type = ShipEquipment.Instance.ShootEquipped;

            SetNormalShootRate();
            StartShooting();
        }


        [ContextMenu("StartShoot")]
        public void StartShooting()
        {
            if (!_isRuning)
            {
                _isRuning = true;
                StartCoroutine(nameof(ShootCoroutine));
            }
        }

        [ContextMenu("StopShoot")]
        public void StopShooting()
        {
            _isRuning = false;
            StopCoroutine(nameof(ShootCoroutine));
        }

        private IEnumerator ShootCoroutine()
        {
            while (_canFire)
            {
                yield return new WaitForSeconds(_currentShootRate);
                Fire();
            }
        }

        private void Fire()
        {
            DoShootSound();

            switch (_type)
            {
                case ShootType.NormalShot:

                    _bullet = PoolManager.Instance.GetFreeObject(Parent, Prefab);

                    _bullet.SetActive(true);
                    _bullet.GetComponent<Bullet>().Initialize(Damage, NormalShootPosition, _type);

                    break;

                case ShootType.DoubleShot:
                    foreach (Transform shootPos in DoubleShootPositions)
                    {
                        _bullet = PoolManager.Instance.GetFreeObject(Parent, Prefab);

                        _bullet.SetActive(true);
                        _bullet.GetComponent<Bullet>().Initialize(Damage, shootPos, _type);
                    }

                    break;

                case ShootType.TripleShot:
                    foreach (Transform shootPos in TripleShootPositions)
                    {
                        _bullet = PoolManager.Instance.GetFreeObject(Parent, Prefab);

                        _bullet.SetActive(true);
                        _bullet.GetComponent<Bullet>().Initialize(Damage, shootPos, _type);
                    }

                    break;

                case ShootType.CurveShot:
                    _bullet = PoolManager.Instance.GetFreeObject(Parent, Prefab);

                    _bullet.SetActive(true);
                    _bullet.GetComponent<Bullet>().Initialize(Damage, NormalShootPosition, _type);

                    CurveBullet curve = _bullet.GetComponent<CurveBullet>();

                    curve.SetStartDirection(_curveDir);
                    _curveDir = !_curveDir;

                    break;

                default:
                    break;
            }
        }

        public void SetNewShootRate(float rate, float time)
        {
            _currentShootRate = rate;
            Invoke(nameof(SetNormalShootRate), time);
        }

        void SetNormalShootRate()
        {
            _currentShootRate = ShootRate;
        }

        private void DoShootSound()
        {
            ShootAS.pitch = UnityEngine.Random.Range(0.8f, 1.2f);
            ShootAS.PlayOneShot(ShootSound);
        }
    }
}