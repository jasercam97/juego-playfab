﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Boundaries : MonoBehaviour
    {
        public UnityEngine.UI.Toggle FitToggle;

        public bool FitSprite;

        private Vector2 _screenBounds;
        private float _objectWidth;
        private float _objectHeight;

        private void Start()
        {
            _screenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
            _objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;
            _objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
        }

        private void LateUpdate()
        {
            Vector3 viewPos = transform.position;

            if (FitSprite)
            {
                viewPos.x = Mathf.Clamp(viewPos.x, _screenBounds.x * -1 + _objectWidth, _screenBounds.x - _objectWidth);
                viewPos.y = Mathf.Clamp(viewPos.y, _screenBounds.y * -1 + _objectHeight, _screenBounds.y - _objectHeight);
            }
            else
            {
                viewPos.x = Mathf.Clamp(viewPos.x, _screenBounds.x * -1, _screenBounds.x);
                viewPos.y = Mathf.Clamp(viewPos.y, _screenBounds.y * -1, _screenBounds.y);
            }

            transform.position = viewPos;
        }

        public void ChangeFitSprite()
        {
            FitSprite = FitToggle.isOn;
        }
    }
}