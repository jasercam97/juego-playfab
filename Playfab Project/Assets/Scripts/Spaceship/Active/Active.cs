﻿using Bullets;
using Enemies;
using System;
using System.Collections;
using System.Collections.Generic;
using UI;
using UnityEngine;
using Utils;
using DG.Tweening;

namespace Player
{
    public class Active : Singleton<Active>
    {
        private ActiveType _equippedActive;

        [Header("Outline")]
        public SpriteRenderer OutlineSprite;
        public float OutlineTweenTime;

        [Space(10)]
        public float DoubleTapTiming;
        public int NumOfUses;
        public float ActiveCooldown;
        public float ChargePerHit;
        public float ChargePerKill;

        [Header("Sound")]
        public AudioSource ActiveAS;
        public AudioClip ActiveChargedSound;
        public AudioClip EnergyPulseSound;
        public AudioClip ShieldActivateSound;
        public AudioClip ShieldDeactivateSound;

        [Header("EnergyPulse")]
        public ParticleSystem EnergyPulseParticles;
        public LayerMask PulseTargetLayer;
        public float EnergyPulseMultiplier;
        private Collider2D[] _colliderList;

        [Header("Invincibility")]
        public PlayerHealth HealthController;
        public Material InvencibleMat;
        public float InvincibilityDuration;
        private Material _normalMat;
        private SpriteRenderer _spriteRenderer;

        [Header("Fast Shoot")]
        [Range(0.05f, 1f)]
        public float NewShootRate;
        public float FastShotDuration;

        private float _usesStored;
        private float _currentCharge;
        private float _maxCharge = 100;

        private Shoot _shoot;

        private bool _firstTap;
        private Tween _outlineTween;

        private void Awake()
        {
            _shoot = GetComponent<Shoot>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _normalMat = _spriteRenderer.material;
        }

        private void Start()
        {
            _equippedActive = ShipEquipment.Instance.ActiveEquipped;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                CheckDoubleTap();
            }
        }

        private void CheckDoubleTap()
        {
            if (_firstTap)
            {
                UseActive();
                CancelInvoke(nameof(CancelDoubleTap));
                _firstTap = false;
            }
            else
            {
                _firstTap = true;
                Invoke(nameof(CancelDoubleTap), DoubleTapTiming);
            }
        }

        private void CancelDoubleTap()
        {
            _firstTap = false;
        }

        private void UseActive()
        {
            if (_usesStored > 0)
            {
                _currentCharge = 0;
                GameUIController.Instance.UpdateHUDActive(_currentCharge / _maxCharge);

                _usesStored--;
                _outlineTween.Kill();
                OutlineSprite.color = Color.clear;

                switch (_equippedActive)
                {
                    case ActiveType.None:
                        break;

                    case ActiveType.EnergyPulse:
                        DoEnergyPulse();
                        break;

                    case ActiveType.Invincibility:
                        DoInvincibility();
                        break;

                    case ActiveType.FastShot:
                        DoFastShot();
                        break;

                    default:
                        break;
                }
            }
        }

        private void DoEnergyPulse()
        {
            ActiveAS.PlayOneShot(EnergyPulseSound);

            Instantiate(EnergyPulseParticles, transform.position, Quaternion.identity);

            _colliderList = Physics2D.OverlapAreaAll(new Vector2(-50, -50), new Vector2(50, 50), PulseTargetLayer);

            foreach (Collider2D collider in _colliderList)
            {
                collider.GetComponent<BasicEnemy>()?.GetHit(_shoot.Damage * EnergyPulseMultiplier);
                collider.GetComponent<Bullet>()?.gameObject.SetActive(false);
            }
        }

        private void DoInvincibility()
        {
            ActiveAS.PlayOneShot(ShieldActivateSound);

            HealthController.ToggleInvincibility();
            _spriteRenderer.material = InvencibleMat;
            Invoke(nameof(StopInvincibility), InvincibilityDuration);
        }

        private void StopInvincibility()
        {
            ActiveAS.PlayOneShot(ShieldDeactivateSound);

            HealthController.ToggleInvincibility();
            _spriteRenderer.material = _normalMat;
        }

        private void DoFastShot()
        {
            ActiveAS.PlayOneShot(ShieldActivateSound);
            _shoot.SetNewShootRate(NewShootRate, FastShotDuration);
        }

        [ContextMenu("AddUse")]
        public void AddActiveUse()
        {
            if(_usesStored < NumOfUses) 
            {
                _usesStored++;
                _outlineTween = OutlineSprite.DOColor(Color.white, 0.75f)
                                             .SetEase(Ease.OutQuad)
                                             .SetLoops(-1, LoopType.Yoyo)
                                             .Play();

                if (_usesStored == NumOfUses)
                {
                    _currentCharge = _maxCharge;
                }
                else
                {
                    _currentCharge = 0;
                }

                ActiveAS.PlayOneShot(ActiveChargedSound);
            }
        }

        public void IncreaseChargeByHit()
        {
            IncreaseCharge(ChargePerHit);
        }

        public void IncreaseChargeByKill()
        {
            IncreaseCharge(ChargePerKill);
        }

        private void IncreaseCharge(float amount)
        {
            _currentCharge += amount;
            GameUIController.Instance.UpdateHUDActive(_currentCharge / _maxCharge);

            if (_currentCharge >= _maxCharge)
            {
                AddActiveUse();
            }
        }
    }
}